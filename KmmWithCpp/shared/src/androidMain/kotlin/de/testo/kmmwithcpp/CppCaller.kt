package de.testo.kmmwithcpp

actual class CppCaller actual constructor() {
  actual fun getStringFromCpp(): String {
    return stringFromJNI()
  }

  /**
   * A native method that is implemented by the 'cplusplusproject' native library,
   * which is packaged with this application.
   */
  external fun stringFromJNI(): String

  companion object {
    // Used to load the 'cplusplusproject' library on application startup.
    init {
      System.loadLibrary("android")
    }
  }
}