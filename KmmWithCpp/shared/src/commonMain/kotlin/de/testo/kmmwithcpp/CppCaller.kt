package de.testo.kmmwithcpp

expect class CppCaller() {
    fun getStringFromCpp(): String
}