package de.testo.kmmwithcpp

class Greeting {
    fun greeting(): String {
        return "Hello, ${Platform().platform} with ${CppCaller().getStringFromCpp()}!"
    }
}