package de.testo.kmmwithcpp

actual class CppCaller {
  actual fun getStringFromCpp(): String {
    return "JVM String but not from c++ yet"
  }
}