package de.testo.kmmwithcpp


actual class Platform actual constructor() {
    actual val platform: String = "JVM " + getOSInfo()

    private fun getOSInfo(): String {
        val name = System.getProperty("os.name")
        val version = System.getProperty("os.version")
        val architecture = System.getProperty("os.arch")

        return "$name $version $architecture"
    }
}