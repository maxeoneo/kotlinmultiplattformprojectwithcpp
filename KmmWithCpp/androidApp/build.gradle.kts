plugins {
    id("com.android.application")
    kotlin("android")
}

dependencies {
    implementation(project(":shared"))
    implementation("com.google.android.material:material:1.4.0")
    implementation("androidx.appcompat:appcompat:1.3.1")
    implementation("androidx.constraintlayout:constraintlayout:2.1.1")
}

android {
    compileSdk = 30
    defaultConfig {
        applicationId = "de.testo.kmmwithcpp.android"
        minSdk = 23
        targetSdk = 30
        versionCode = 1
        versionName = "1.0"
      externalNativeBuild {
        cmake {
          cppFlags += ""
        }
      }
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
        }
    }
  externalNativeBuild {
    cmake {
      path = file("src/main/cpp/CMakeLists.txt")
      version = "3.10.2"
    }
  }
}