#include <jni.h>
#include <string>

#include "../../../../shared/src/main/cpp/CommonFunctions.h"

extern "C"
JNIEXPORT jstring JNICALL
Java_de_testo_kmmwithcpp_CppCaller_stringFromJNI(JNIEnv *env, jobject thiz) {
  std::string hello = CommonFunctions::getStringFromCommon();
  return env->NewStringUTF(hello.c_str());
}