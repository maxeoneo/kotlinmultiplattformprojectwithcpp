# KotlinMultiPlattformProjectWithCpp

This project contains an example project how a kotlin app could work for all plattforms. There are different run configurations for Android, iOS and JVM.

There are also different modules for each plattform app and a shared modules, which also contains different submodules for plattform specific code that could be used from the shared code.

- Project
  - androidApp
  - iosApp
  - vmApp
  - shared
    - android
    - common
    - ios
    - vm


Missing Steps to find out:
 - Call c++ code from Kotlin for iOS and JVM -> Roadmap of Kotlin promises a better c++ integration
 - Find out if it is possible to call different QtActivities. Then we could structure out app in different Activities (and corresponding stuff for iOS) and replace it one after another with kotlin
   We also would gain common plattform features, like the back button would work automatically for activity switches or we could use "App shortcuts" for Android, which means that a user could define a short could right on the android home screen that would start a defined process as adhoc task in the "process engine activity"
 - use couchbase from common kotlin code
 - find out if jetbrains compose can be used for android AND JVM UI -> save one UI development
